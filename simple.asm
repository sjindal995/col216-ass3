.main: 
.encode	mov r0, 10
.encode	call .factorial
.encode	b .break
.factorial: .encode	cmp r0, 1
.encode	bgt .continue
.encode	mov r1, 1
.encode	ret	
.continue: .encode		sub sp, sp, 8
.encode		st r0, [sp]
.encode		st ra, 4[sp]
.encode		sub r0, r0 , 1 
.encode		call .factorial
.encode		ld ra, 4[sp]
.encode		ld r0 ,  [sp]
.encode		add sp,sp, 8
.encode		mul r1, r0,r1
.encode		ret
.break: .encode nop
.encode nop